<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ipc_login_validation {
   	/**
	* @desc check if user has logged in
	* @return bool
	*/
	public function is_logged_in(){
		$CI =& get_instance();
		if( !$CI->session->has_userdata('ipc_logged_in') ){
			$is_logged_in = $CI->session->userdata('ipc_logged_in');
			if(!$is_logged_in){
				redirect(base_url('../webapps'));
			}
		}//endif
		return true;
	}//end is_logged_in()
}