<!--Begin::Main Portlet-->
<div class="row">
    <div class="col-xl-12">
        <!--begin::Portlet-->
        <div class="m-portlet" id="m_portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="flaticon-users"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            User
                        </h3>
                        <!--begin: Create User Button -->
                        <div class="m-input-icon m-input-icon--right m-input-header">
                        <!-- <input type="text" class="form-control " placeholder="Search..." id="user-search"> -->
                            <a href="<?php echo base_url('user/manage/create'); ?>" class="btn btn-primary">Create User</a>
                        </div>
                        <!--end: Create User Button -->
                    </div> 
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <?php if( $this->session->flashdata('msg') ): ?>
                        <div class="alert alert-<?php echo $this->session->flashdata('msg')['alert_type']; ?> alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                            <?php echo $this->session->flashdata('msg')['msg']; ?>
                        </div>
                    <?php endif;?>
                </div>
            </div>
            <!--begin: User Datatable -->
            <table id="user-datatables" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th title="Field #1">
                            User ID
                        </th>
                        <th title="Field #2">
                            Name
                        </th>
                        <th title="Field #3">
                            Division
                        </th>
                        <th title="Field #4">
                            Department
                        </th>
                        <th title="Field #5">
                            Section
                        </th>
                        <th title="Field #6">
                            User Source ID
                        </th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($users as $user): ?>
                        <tr class="<?php echo ($user->STATUS_ID != 1?'inactive':''); ?>" 
                            <?php echo ($user->STATUS_ID != 1?'data-placement="bottom" data-toggle="tooltip" title="Inactive User!"':''); ?>
                        >
                            <td>
                                <?php echo $user->USER_ID; ?>
                            </td>
                            <td>
                                <?php echo $user->FIRST_NAME.' '.$user->MIDDLE_NAME.' '.$user->LAST_NAME; ?>
                            </td>
                            <td>
                                <?php echo $user->DIVISION; ?>
                            </td>
                            <td>
                                <?php echo $user->DEPARTMENT; ?>
                            </td>
                            <td>
                                <?php echo $user->SECTION; ?>
                            </td>
                            <td>
                                <?php echo ($user->USER_SOURCE_ID == 1?'oracle':'ipc_portal'); ?>
                            </td>
                            <td>
                                <?php $edit_link = ($user->USER_SOURCE_ID==2?base_url('/user/manage/edit/'.$user->USER_ID):"#"); ?>
                                <?php if($user->USER_SOURCE_ID==2):?>
                                    <a href="<?php echo $edit_link; ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill hidden" title="<?php echo ($user->USER_SOURCE_ID==1)?'Edit not allowed for oracle users':'Edit User' ?>">                            
                                        <i class="la la-edit"></i>
                                    </a>
                                <?php endif; ?>
                                <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Manage System Access" data-toggle="modal" data-target="#m_access_modal">                            
                                    <i class="la la-gear"></i>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach ;?>    
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
        <!--end::Portlet-->
    </div>
</div>
<!--End::Main Portlet-->
<div class="modal" id="m_access_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Manage System Access
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <h3 id="user_name">John Joey C. Abuedo</h3>
                </div>
                <div class="col-md-12">
                    <h5 id="division_name">PHP Developer</h5>
                </div>
                <div id="system_list_main_wrapper">
                    <table id="user_system_access_tbl" class="table m-table m-table--head-bg-success">
                        <thead>
                            <tr>
                                <th>
                                    Access
                                </th>
                                <th>
                                    System
                                </th>
                                <th>
                                    User Type
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($systems as $system): ?>
                                <tr class="access_tr_main_wrapper">
                                    <td>
                                        <div class="bootstrap-switch-container" style="width: 102px;">
                                            <input class="switch_system_access" id="switch_system_access<?php echo $system->SYSTEM_ID; ?>" type="checkbox" checked="checked" data-size="mini" data-system-id = "<?php echo $system->SYSTEM_ID; ?>">
                                        </div>
                                    </td>
                                    <td>
                                        <?php echo $system->SYSTEM_NAME; ?>
                                    </td>
                                    <td>
                                        <select id="user_types<?php echo $system->SYSTEM_ID; ?>" class="form-control user_types" name="user_types">
                                            <?php foreach( $system_usertypes as $key => $value ): ?>

                                                <?php foreach($value as $key => $val): ?>

                                                    <?php if($key == 'SYSTEM_ID'): ?>

                                                        <?php if($value->SYSTEM_ID == $system->SYSTEM_ID ): ?>
                                                            <option value="<?php echo $value->USER_TYPE_ID; ?>"><?php echo $value->USER_TYPE_NAME; ?></option>
                                                        <?php endif;?>    

                                                    <?php endif; ?>

                                                <?php endforeach;?>

                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            <tr style="display: none;" id="access_loader"> 
                                <td>Loading...</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
           <!--  <div class="modal-footer">
                <button type="button" class="btn btn-primary">
                    Save
                </button>
            </div> -->
        </div>
    </div>
</div>