<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">User Management</a></li>
    <li class="breadcrumb-item"><a href="<?php echo base_url('/user'); ?>">User List</a></li>
    <li class="breadcrumb-item active"><?php echo ucfirst($action); ?> User</li>
</ol>
<!--Begin::Main Portlet-->
<div class="row">
    <div class="col-xl-12">
        <!--begin::Portlet-->
        <div class="m-portlet" id="m_portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon">
                            <i class="flaticon-user-add"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            <?php echo $form_title; ?>
                        </h3>
                    </div>
                </div>
            </div>
            <?php echo form_open('user/manage/'.$action.'/'.$user_id, array( 'class' => 'm-form m-form--fit m-form--label-align-left') ); ?>
                <div class="form-main-wrapper">
                    
                    <?php if( $this->session->flashdata('msg') ): ?>
                        <div class="form-group m-form__group row">
                            <div class="alert alert-<?php echo $this->session->flashdata('msg')['alert_type']; ?> alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                                <?php echo $this->session->flashdata('msg')['msg']; ?>
                            </div>
                        </div>
                    <?php endif;?>
                    
                    <?php if( strtoupper($action) == strtoupper('edit') ) :?>
                    <div class="form-group m-form__group row">
                        <label for="USER_NAME" class="col-2 col-form-label">
                            Status
                        </label>
                        <div class="col-10">
                            <span class="m-switch m-switch--icon m-switch--success">
                                <label>
                                    <input type="hidden" class="form-control" name="STATUS_ID" value="2" <?php echo ($user_data['STATUS_ID'] == 1?'':'checked'); ?>>
                                        <input type="checkbox" class="form-control" name="STATUS_ID" value="1" <?php echo ($user_data['STATUS_ID'] == 1?'checked':'') ?> >
                                    <span></span>
                                </label>
                            </span>
                        </div>
                    </div>
                    <?php else: ?>
                        <div class="m-form__group"></div>
                    <?php endif;?>
                    <div class="form-group m-form__group row">
                        <label for="FIRST_NAME" class="col-2 col-form-label">
                            Firstname
                        </label>
                        <div class="col-10">
                            <input autofocus class="form-control m-input" type="text" name="FIRST_NAME" id="FIRST_NAME" required value="<?php echo (isset($user_data)?$user_data['FIRST_NAME']: ''); ?>">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="MIDDLE_NAME" class="col-2 col-form-label">
                            Middlename
                        </label>
                        <div class="col-10">
                            <input class="form-control m-input" type="text" name="MIDDLE_NAME" id="MIDDLE_NAME" required value="<?php echo (isset($user_data)?$user_data['MIDDLE_NAME']: ''); ?>">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="LAST_NAME" class="col-2 col-form-label">
                            Lastname
                        </label>
                        <div class="col-10">
                            <input class="form-control m-input" type="text" name="LAST_NAME" id="LAST_NAME" required value="<?php echo (isset($user_data)?$user_data['LAST_NAME']: ''); ?>">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="SECTION" class="col-2 col-form-label">
                            Section
                        </label>
                        <div class="col-10">
                            <input class="form-control m-input" type="text" name="SECTION" id="SECTION" required value="<?php echo (isset($user_data)?$user_data['SECTION']: ''); ?>">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="DEPARTMENT" class="col-2 col-form-label">
                            Department
                        </label>
                        <div class="col-10">
                            <input class="form-control m-input" type="text" name="DEPARTMENT" id="DEPARTMENT" required value="<?php echo (isset($user_data)?$user_data['DEPARTMENT']: ''); ?>">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="DIVISION" class="col-2 col-form-label">
                            Division
                        </label>
                        <div class="col-10">
                            <input class="form-control m-input" type="text" name="DIVISION" id="DIVISION" required value="<?php echo (isset($user_data)?$user_data['DIVISION']: ''); ?>">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="ORGANIZATION" class="col-2 col-form-label">
                            Organization
                        </label>
                        <div class="col-10">
                            <input class="form-control m-input" type="text" name="ORGANIZATION" id="ORGANIZATION" required value="<?php echo (isset($user_data)?$user_data['ORGANIZATION']: ''); ?>">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="POSITION_TITLE" class="col-2 col-form-label">
                            Position Title
                        </label>
                        <div class="col-10">
                            <input class="form-control m-input" type="text" name="POSITION_TITLE" id="POSITION_TITLE" required value="<?php echo (isset($user_data)?$user_data['POSITION_TITLE']: ''); ?>">
                        </div>
                    </div>
                                        <div class="form-group m-form__group row">
                        <label for="USER_NAME" class="col-2 col-form-label">
                            Username
                        </label>
                        <div class="col-10">
                            <input class="form-control m-input" type="text" name="USER_NAME" id="USER_NAME" required value="<?php echo (isset($user_data)?$user_data['USER_NAME']: ''); ?>">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="PASSCODE" class="col-2 col-form-label">
                            Passcode
                        </label>
                        <div class="col-10">
                            <input class="form-control m-input" type="password" name="PASSCODE" id="PASSCODE" required value="<?php echo (isset($user_data)?$user_data['PASSCODE']: ''); ?>">
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label for="CONFIRM_PASSCODE" class="col-2 col-form-label">
                            Confirm Passcode
                        </label>
                        <div class="col-10">
                            <input class="form-control m-input" type="password" name="CONFIRM_PASSCODE" id="CONFIRM_PASSCODE" required value="<?php echo (isset($user_data)?$user_data['CONFIRM_PASSCODE']: ''); ?>">
                        </div>
                    </div>
                     <div class="form-group m-form__group row"></div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <input type="submit" class="btn btn-success" value="Submit" name="submit">
                        <button type="reset" class="btn btn-primary">
                            Clear
                        </button>
                    </div>
                </div>
            <?php echo form_close(); ?>    
        </div>
        <!--end::Portlet-->
    </div>
</div>
<!--End::Main Portlet-->