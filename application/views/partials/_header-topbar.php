<!-- BEGIN: Topbar -->
<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
	<div class="m-stack__item m-topbar__nav-wrapper">
		<ul class="m-topbar__nav m-nav m-nav--inline">
			<!-- end::USER PROFILE -->
				<?php $this->load->view('partials/_topbar-user-profile'); ?>
			<!-- end::USER PROFILE -->
		</ul>
	</div>
</div>
<!-- END: Topbar -->
