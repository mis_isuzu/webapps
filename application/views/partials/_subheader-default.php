<!-- BEGIN: Subheader -->
<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title ">
				<?php echo ($content_title?$content_title:''); ?>
			</h3>
		</div>
	</div>
</div>
<!-- END: Subheader -->