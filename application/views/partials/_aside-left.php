<?php 

//count all allowed items for the current user
$sys_count = 0;
if(isset($user_access)){
	if($user_access){
		foreach ($user_access as $access) {
			if($access->STATUS_ID ==1 ){
				$sys_count ++;
			}
		}		
	}//endif
}//endif

?>

<!-- BEGIN: Left Aside -->
<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
	<i class="la la-close"></i>
</button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
	<!-- BEGIN: Aside Menu -->
	<div 
		id="m_ver_menu" 
		class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " 
		data-menu-vertical="true"
		 data-menu-scrollable="false" data-menu-dropdown-timeout="500"  
		>
		<ul class="m-menu__nav m-menu__nav--dropdown-submenu-arrow ">
			<?php if(isset($user_access)): ?>
				<?php if( $user_access ): ?>
					<?php foreach($user_access as $access): ?>
						<?php if($access->SYSTEM_ID == 3 && $access->STATUS_ID == 1): ?> 
							<li class="m-menu__item  m-menu__item--active" aria-haspopup="true" >
								<a  href="<?php echo base_url('/dashboard'); ?>" class="m-menu__link ">
									<i class="m-menu__link-icon flaticon-line-graph"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">
												Dashboard
											</span>
											<span class="m-menu__link-badge">
												<span class="m-badge m-badge--danger" title="<?php echo $sys_count; ?> systems allowed" data-toggle="tooltip" title="Hooray!">
													<?php echo $sys_count; ?> 
												</span>
											</span>
										</span>
									</span>
								</a>
							</li>
							<li class="m-menu__section">
								<h4 class="m-menu__section-text">
									MENU
								</h4>
								<i class="m-menu__section-icon flaticon-more-v3"></i>
							</li>
							<li 
								class="m-menu__item  m-menu__item--submenu 
									<?php echo ($page_title == 'User Management' || 
												$page_title == 'Create User' ||
												$page_title == 'Edit User'
												?'m-menu__item--expanded m-menu__item--open':''); ?>" 
								aria-haspopup="true"  data-menu-submenu-toggle="hover">
								<a  href="#" class="m-menu__link m-menu__toggle">
									<i class="m-menu__link-icon flaticon-users"></i>
									<span class="m-menu__link-text">
										User Management
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item <?php echo ($page_title == 'User Management' ?'m-menu__item--active':''); ?>" aria-haspopup="true">
											<a  href="<?php echo base_url('/user'); ?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													List
												</span>
											</a>
										</li>
										<li class="m-menu__item <?php echo ($page_title == 'Create User' ?'m-menu__item--active':''); ?>" aria-haspopup="true" >
											<a href="<?php echo base_url('/user/manage/create'); ?>" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													Create New
												</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
						<?php endif;?>
					<?php endforeach; ?>
				<?php endif;?>
			<?php endif;?>
		</ul>
	</div>
	<!-- END: Aside Menu -->
</div>
<!-- END: Left Aside -->