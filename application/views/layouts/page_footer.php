<footer>
	<!-- if the script is for specific page only, use if statement -->
	<?php //if( $page_title == 'Sample Page' ): ?>
		<!-- <script src="sample_script.js"></script> -->
	<?php //endif;?>
	<script src="<?php echo base_url('assets/js/jquery.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/libraries/metronic/assets/vendors/base/vendors.bundle.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/libraries/metronic/assets/demo/default/base/scripts.bundle.js');?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/js/webfont.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/js/datatables.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/js/bootstrap4.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/js/user_datatables.js'); ?>" type="text/javascript"></script>
    <script>
		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip(); 
		});
	</script>
	<script>
	    WebFont.load({
	        google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
	        active: function() {
	            sessionStorage.fonts = true;
	        }
	    });
	</script>
	<script>
		BASE_URL = "<?php echo base_url();?>";
	</script>
	<script src="<?php echo base_url('assets/js/main.js');?>" type="text/javascript"></script>
</footer>