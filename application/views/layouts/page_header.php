<meta charset="utf-8" />
<meta name="description" content="Latest updates and statistic charts">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!--begin::Base Styles -->  

<!-- Start::SASS -->
<link href="<?php echo base_url('assets/css/stylesheets/main.css'); ?>" rel="stylesheet" type="text/css" />
<!-- Start::SASS -->
<link rel="shortcut icon" type='image/x-icon' href="<?php echo base_url('assets/images/favicon.ico'); ?>" />