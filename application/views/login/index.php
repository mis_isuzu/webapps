<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="author" content="Chris Desiderio">
		<title>WebApps</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap core CSS -->
		<link href="<?php echo base_url('assets/libraries/bootstrap-3.3.7/css/bootstrap.min.css');?>" rel="stylesheet">
		<!-- Custom styles for this template -->
		<link href="<?php echo base_url('assets/css/login.css');?>" rel="stylesheet">
		<link rel="shortcut icon" type='image/x-icon' href="<?php echo base_url('assets/images/favicon.ico'); ?>" />
	</head>
	<body class="page-login-v2 layout-full page-dark">
		<div class="page animsition" style="opacity: .85; animation-duration: 800ms;">
			<div class="page-content">
				<div class="page-brand-info">
					<div class="brands">
						<h2 class="brand-text font-size-40 font-weight-400"></h2>
					</div>
				</div>
				
				<div class="page-login-main">
					<div class="brand" style="text-align: center;">
						<img class="brand-img" style="margin-bottom: 10px;" src="<?php echo base_url('assets/images/logo_white.png') ?>" alt="...">
						<p style="font-size: 15px;">WebApps</p>
					</div>
					<form id="login-form" role="form">
						<h3 style="font-size: 15px;">Sign in to start your session</h3>
						
						<!--ERROR MESSAGE-->
						<div class="alert alert-warning login-error">
							<h4>Login Failed!</h4>
							 Either your username or password is incorrect.
						</div>
						
						<div class="form-group">
							<input type="text" class="form-control" id="p_username" placeholder="Username" autofocus>
						</div>
						<div class="form-group">
							<input type="password" class="form-control" id="p_password" placeholder="Password">
						</div>
			 
						<button type="submit" id="signin_btn" class="btn btn-danger btn-flat btn-block">Sign in</button>
						<p></p>
					</form>
					<footer class="page-copyright">
						<p>Management Information System</p>
						<p>© 2017. All Rights Reserved.</p>
					</footer>
				</div>
			</div>
		</div>
		<!-- Change Password Modal -->
		<div class="modal fade" id="change_password_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="color:#000;">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h3 class="modal-title" id="exampleModalLabel">Change Password</h3>
				</div>
				<div class="modal-body">
					<div id="modal-message" class="alert alert-warning">
						<h5></h5>
					</div>
					<div id="change_pw_form_container">			
						<form id="change_pw_form">						
							<div class="form-group">
								<input type="password" placeholder="Old Password" class="form-control cp_input" id="old_password" autofocus>
							</div>
							<div class="form-group">
								<input type="password" placeholder="New Password" class="form-control cp_input" id="new_password">
							</div>
							<div class="form-group">
								<input type="password" placeholder="Confirm New Password" class="form-control cp_input" id="confirm_password">
							</div>
						</form>
					</div>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-primary" value="Submit" id="change_pw_btn" onclick="user_change_pw(this)">
					<input type="button" class="btn btn-secondary" data-dismiss="modal" value="Cancel">
				</div>
				</div>
			</div>
		</div>

		<!-- Change Password Modal Message -->
		<div class="modal fade" id="change_password_modal_msg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="color:#000;">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h3 class="modal-title" id="exampleModalLabel">Change Password</h3>
				</div>
				<div class="modal-body">
					<div id="modal-message">
						<h5></h5>
					</div>
				</div>
				<div class="modal-footer">
					<input type="button" class="btn btn-secondary" data-dismiss="modal" value="Login">
				</div>
				</div>
			</div>
		</div>
	</body>
</html>

<script>
	BASE_URL = "<?php echo base_url(); ?>";
</script>
<script src="<?php echo base_url('assets/js/jquery.js');?>"></script>
<script src="<?php echo base_url('assets/libraries/bootstrap-3.3.7/js/bootstrap.min.js');?>"></script>
<!-- <script src="<?php //echo base_url('assets/js/form_validation.js');?>"></script> -->
<script>
	$(document).ready(function() {		
		$('.alert.login-error').hide();
		$('#login-form').submit(function(e){
			e.preventDefault();
			var p_username = $('#p_username').val();
			var p_password = $('#p_password').val();

			// x = new FormValidation($(this));
			// x.validate();
			//return true;
			$.ajax({
				type:'POST',
				dataType: 'json',
				data:{
					username : p_username,
					password : p_password
				},
				url:BASE_URL+'login/validate',
				cache: false,
				beforeSend: ajax_loader('#signin_btn', 'please wait...', 'show'),
				success:function(response){
					if( response['status'] ){
						window.location.assign(BASE_URL+'dashboard');
					}else{
						if(response['code'] == 404){ //user not found

							$('.alert.login-error').hide();
							$('.alert.login-error').html(response['msg']);
							$('.alert.login-error').show('slow');

						}else if(response['code'] == 401){ // user password expired already
							var username = response['data']['USER_NAME'];

							$('form#change_pw_form')[0].reset();
							$('#change_password_modal .modal-body #modal-message h5').html(response['msg']);
							$('#change_password_modal').modal('show');
							$('#change_password_modal .modal-footer #change_pw_btn').attr('data-username', username);

						}//endif
					}//endif
				},
				complete: ajax_loader('#signin_btn', 'Sign in', 'hide')
			});
		});
	});

	//loader for button(please wait...)
	function ajax_loader(btnSelector, label, action){
		switch(action){
			case 'show':
				$(btnSelector).html(label);	
				break;
			case 'hide':
				$(document).ajaxStop(function() {
					$(btnSelector).html(label);	
				});
				break;
		}
	}//end ajax_loader()

	//request to change password for specific user
	function user_change_pw(me){
		var user_name 			= $(me).attr('data-username');
		var old_password 		= $('#change_pw_form_container').find('#old_password').val();
		var new_password 		= $('#change_pw_form_container').find('#new_password').val();
		var confirm_password 	= $('#change_pw_form_container').find('#confirm_password').val();
		data = {
			user_name: user_name,
			old_password: old_password,
			new_password: new_password,
			confirm_password: confirm_password,
		};
		if( change_pw_validation() ){
			$.ajax({
				type:'POST',
				dataType: 'JSON',
				url: BASE_URL+ 'login/change_pw',
				cache: false,
				data: data,
				beforeSend: ajax_loader('#change_pw_btn', 'please wait...', 'show'),
				success: (function(response){

					if(response['status']){
						show_change_pw_msg(response['msg'], 'success');
					}else{
						show_change_pw_msg(response['msg'], 'danger');
					}//endif

				}),
				complete: ajax_loader('#change_pw_btn', 'Submit', 'hide')
			});
		}else{
			return false;
		}//endif
	}

	function change_pw_validation(){
		var err_counter 		= 0;
		var oldPasswordEl 		= $('#change_pw_form_container').find('#old_password');
		var newPasswordEl 		= $('#change_pw_form_container').find('#new_password');
		var confirmPasswordEl 	= $('#change_pw_form_container').find('#confirm_password');

		var oldPasswordVal 		= oldPasswordEl.val();
		var newPasswordVal 		= newPasswordEl.val();
		var confirmPasswordVal 	= confirmPasswordEl.val();

		//check each input if there is a value
		$('#change_pw_form_container').find('.cp_input').each(function(){
			var curVal = $(this).val();
			if( curVal == '' || curVal == null ){
				$(this).tooltip({
								placement: 'left',
								title: 'This field is required.'
							}).tooltip('show');
				err_counter++;
				return false;
			}
			$(this).tooltip('destroy');
		});

		//check if old and new password inputs are the same value
		if( (oldPasswordVal == newPasswordVal) && (oldPasswordVal != '' && newPasswordVal != '') ){
	
			newPasswordEl.tooltip({
								placement: 'left',
								title: 'New password must be different from old password.'
							}).tooltip('show');
			err_counter++;

		//check if new and confirm password inputs are the same value	
		}else if( (newPasswordVal != confirmPasswordVal) && (oldPasswordVal != '' && newPasswordVal != '') ){

			confirmPasswordEl.tooltip({
								placement: 'left',
								title: 'New password and Confirm password must be same.'
							}).tooltip('show');
			err_counter++;
		}//endif
		return (err_counter>0?false:true);
	}

	function show_change_pw_msg(msg, alertType){
		$('#change_password_modal').modal('hide');
		$('#change_password_modal_msg .modal-body #modal-message').removeClass();
		$('#change_password_modal_msg .modal-body #modal-message').addClass('alert alert-'+alertType);
		$('#change_password_modal_msg .modal-body #modal-message').html(msg);
		$('#change_password_modal_msg').modal('show');
	}
</script>
