<div class="row">
	<?php $count=0; ?>	
	<?php if($systems):?>
		<?php foreach($systems as $system): ?>		
			<?php if($user_access):?>
				<?php foreach ($user_access as $access):?>
					<?php if ($access->SYSTEM_ID == $system->SYSTEM_ID):?>
						<?php if($access->STATUS_ID == 1):?>
							<?php 
								$count++;
								$bg_class = ($count % 2 == 0 ? 'm--bg-danger' : 'm--bg-accent'); 
							?>	
							<div class="col-md-4">
								<div class="m-portlet m-portlet--bordered-semi m-portlet--skin-dark <?php echo $bg_class; ?>">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<h3 class="m-portlet__head-text">
													<a href="<?php echo base_url('/../').$system->URL; ?>" class="text-white" target="_blank">
														<?php echo $system->SYSTEM_NAME; ?>
													</a>
												</h3>
											</div>
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
												<li class="m-portlet__nav-item" data-dropdown-toggle="hover" aria-expanded="true">
													<a href="<?php echo base_url('/../').$system->URL; ?>" class="m-portlet__nav-link btn btn-secondary m-btn m-btn--air m-btn--icon m-btn--icon-only m-btn--pill" target="_blank">
														<i class="la la-angle-right"></i>
													</a>
												</li>
											</ul>
										</div>
									</div>
								</div>	
							</div>
						<?php endif;?>
					<?php endif;?>
				<?php endforeach;?>
			<?php else: ?>
				<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-danger show" role="alert">
					<div class="m-alert__icon">
						<i class="la la-warning"></i>
					</div>
					<div class="m-alert__text">
						<strong>
							Uh-oh! You do not have access to this system.
						</strong>
					</div>
				</div>	
				<?php break; ?>
			<?php endif;?>
		<?php endforeach; ?>
	<?php else: ?>
		<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-success show" role="alert">
			<div class="m-alert__icon">
				<i class="la la-warning"></i>
			</div>
			<div class="m-alert__text">
				<strong>
					Uh-oh! There is no system found in database.
				</strong>
			</div>
		</div>	
	<?php endif;?>
</div>