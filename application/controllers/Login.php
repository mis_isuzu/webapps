<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('login_model', 'm_login');
		$this->load->model('user_model', 'm_user');
	} //end construct()

	/**
	* @desc Index Page for this controller.
	* @author John Joey C. Abuedo | IPC
	* @see link https://mis_isuzu@bitbucket.org/mis_isuzu/webapps.git 
	* @return view
	*/
	public function index() {
		$this->is_logged_in();
		$this->load->view('login/index');
	} //end index()

	/**
	* @desc validate user credentials
	* @param string username
	* @param string password
	* @return json
	*/
	public function validate() {
		$username 	= $this->input->post('username');
		$password 	= $this->input->post('password');
		$creds_arr  = array(
							$username,
		 					$password,
							$username,
		 					$password
		 				);
		$val_res 	= $this->m_login->validate_user($creds_arr);
		//check if user exists
		if( $val_res ) {
			$user_status = $val_res->STATUS_ID;
			if($user_status == 1){
				$date_now 			= strtotime(date("Y-m-d"));
				$password_expiry 	= strtotime(date("Y-m-d", strtotime($val_res->PASSWORD_EXPIRATION)));
				if( $date_now >= $password_expiry && $val_res->USER_SOURCE_ID == 2 ){ // 1=oracle users, 2=ipc_portal users

					$res = array(
									'status' 	=> false, 
									'code'		=> 401, // unauthorized as the password already expired
									'msg' 		=> 'It seems your password already expired. Please change your password and login again.',
									'data' 		=> $val_res
								);

				}else{

					$this->login($val_res);
					$res = array(
									'status' 	=> true,
					 				'code'		=> 200, // success request
					 				'msg' 		=> 'Successfully logged in.',
					 				'data' 		=> []
					 			);

				}//endif
			}else{
				$res = array(
								'status' 	=> false,
							 	'code' 		=> 404, //user not found due to account was deactivated
							  	'msg' 		=> 'Your account is deactivated. Please contact system admin to retrieve your account.',
							  	'data' 		=> []
						  	);
			}//endif
		}else{
			$res = array(
							'status' 	=> false,
						 	'code' 		=> 404, //user not found
						  	'msg' 		=> '<h4>Login Failed!</h4> Either your username or password is incorrect.',
						  	'data' 		=> []
					  	);
		}//endif
		echo json_encode($res);exit;
	} //end validate()

	/**
	* @desc save user credentials to session
	* @param array user_data
	* @return array
	*/
	private function login($user_data) {
		$newdata = array(
						'employee_number' 	=> $user_data->EMPLOYEE_NUMBER,
						'user_source_id' 	=> $user_data->USER_SOURCE_ID,
				        'user_id'  			=> $user_data->USER_ID,
				        'username'  		=> $user_data->USER_NAME,
				        'full_name'			=> $user_data->FIRST_NAME.' '.$user_data->MIDDLE_NAME.' '.$user_data->LAST_NAME,
				        'ipc_logged_in' 	=> TRUE
					);
		$this->session->set_userdata($newdata);
	} //end set_user_data()

	/**
	* @desc logout and delete user session
	* @return view
	*/
	public function logout() {
		$this->session->sess_destroy();
		redirect(base_url());
	}

   	/**
	* @desc check if user is logged in
	* @return bool
	*/
	private function is_logged_in() {
		if( $this->session->has_userdata('ipc_logged_in') ) {
			$is_logged_in = $this->session->userdata('ipc_logged_in');
			if($is_logged_in) {
				redirect('dashboard');
			}
		} //endif
		return true;
	} //end is_logged_in()

	/**
	* @desc change password for specific user
	* @param string user_name
	* @param string old_password
	* @param string new_password
	* @param string confirm_password
	* @return json
	*/
	public function change_pw(){
		$change_pw_data = $this->input->post();
		$user_name 		= $this->input->post('user_name');
		$old_password 	= $this->input->post('old_password');
		
		$this->form_validation->set_rules('user_name', 'Username', 'trim|required');
		$this->form_validation->set_rules('old_password', 'Old Passcode', 'trim|required');
		$this->form_validation->set_rules('new_password', 'New Passcode', 'trim|required');
		$this->form_validation->set_rules('confirm_password', 'Passcode Confirmation', 'trim|required|matches[new_password]');

		if ( $this->form_validation->run() == FALSE ){
			$res = array(
					'status' 	=> false,
				 	'code' 		=> 400, //user not found
				  	'msg' 		=> validation_errors(),
				  	'data' 		=> []
			  	);
        }else{    	
			if( $this->has_user($user_name) ){
				$creds_arr  = array( $user_name, $old_password, $user_name, $old_password );
				$val_res 	= $this->m_login->validate_user($creds_arr);

				if($val_res){
					$change_pw_res = $this->m_login->change_pw($change_pw_data);
					if($change_pw_res){
						$res = array(
							'status' 	=> true,
						 	'code' 		=> 200, //change password success
						  	'msg' 		=> 'You have successfully changed your password. Please login with your new password.',
						  	'data' 		=> []
					  	);
					}else{
						$res = array(
							'status' 	=> false,
						 	'code' 		=> 400, //bad request
						  	'msg' 		=> 'Something went wrong.',
						  	'data' 		=> []
					  	);					
					}
				}else{
					$res = array(
						'status' 	=> false,
					 	'code' 		=> 404, //user not found
					  	'msg' 		=> 'Invalid old password.',
					  	'data' 		=> []
				  	);
				}//endif
			}else{
				$res = array(
					'status' 	=> false,
				 	'code' 		=> 404, //user not found
				  	'msg' 		=> 'Username not found in database.',
				  	'data' 		=> []
			  	);
			}
        }
		echo json_encode($res);exit;

	}//end change_password()

	/**
	* @desc check username if already exists
	* @param string $username
	* @return bool
	*/
	public function has_user($username){
		if($username){
			$check_res = $this->m_user->has_user($username);
			return ($check_res?true:false);
		}//endif
	}//end check_user()
	
}//end Login class
