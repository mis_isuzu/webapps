<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User extends MY_Controller {

	public function __construct(){
		parent::__construct();
	//	$this->ipc_login_validation->is_logged_in();
		$this->load->model('user_model', 'm_user');
		$this->load->model('system_model', 'm_system');
		$this->check_system_permission();
	}//end __construct()

	/**
	* @desc Index Page for this controller.
	* @author John Joey C. Abuedo | IPC
	* @see link https://mis_isuzu@bitbucket.org/mis_isuzu/webapps.git 
	* @return view
	*/
	public function index(){
		$user_id						= $this->session->userdata('user_id'); 
		$source_id 						= $this->session->userdata('user_source_id'); 
		$page_data['users'] 			= $this->m_user->get_all_user();
		$page_data['system_usertypes'] 	= $this->m_system->get_system_usertypes();
		$page_data['systems'] 			= $this->m_system->get_system();
		$page_data['user_access']  		= $this->m_system->get_user_system_access($user_id, $source_id);
		$page_data['page_content']  	= 'user/index';
		$page_data['page_title']  		= 'User Management';
		$page_data['content_title'] 	= 'User List';
		$this->load->view('layouts/page_template', $page_data);
	}//end index()

	/**
	* @desc create user
	* @return view
	*/
	public function manage($action = null, $user_id = null){
		if( strtoupper($action) == strtoupper('create') || strtoupper($action) == strtoupper('edit') ){
			$loggedin_u_id				= $this->session->userdata('user_id'); 
			$loggedin_source_id 		= $this->session->userdata('user_source_id'); 
			$page_data['user_access']  	= $this->m_system->get_user_system_access($loggedin_u_id, $loggedin_source_id);
			$page_data['action'] 		= $action;
			$page_data['page_content']  = 'user/form';
			$page_data['page_title']  	= (strtoupper($action) === strtoupper('create')?'Create':'Edit').' User';
			$page_data['form_title']  	= $page_data['page_title'];
			$page_data['content_title'] = 'User Management';
			$page_data['user_data'] 	= (strtoupper($action) === strtoupper('edit')?(array) $this->m_user->get_user($user_id):false);
			$page_data['user_id'] 		= $user_id;

			if( $this->input->post('submit') ){

				$this->form_validation->set_rules('USER_NAME', 'Username', 'trim|required');
				$this->form_validation->set_rules('PASSCODE', 'Passcode', 'trim|required');
				$this->form_validation->set_rules('CONFIRM_PASSCODE', 'Passcode Confirmation', 'trim|required|matches[PASSCODE]');
				$this->form_validation->set_rules('FIRST_NAME', 'First Name', 'trim|required');
				$this->form_validation->set_rules('MIDDLE_NAME', 'Middle Name', 'trim|required');
				$this->form_validation->set_rules('LAST_NAME', 'Last Name', 'trim|required');
				$this->form_validation->set_rules('SECTION', 'Section', 'trim|required');
				$this->form_validation->set_rules('DEPARTMENT', 'Department', 'trim|required');
				$this->form_validation->set_rules('DIVISION', 'Division', 'trim|required');
				$this->form_validation->set_rules('ORGANIZATION', 'Organization', 'trim|required');
				$this->form_validation->set_rules('POSITION_TITLE', 'Position Title', 'trim|required');

				if ( $this->form_validation->run() == FALSE ){
	                $this->flash_message('danger', validation_errors());
					$page_data['user_data'] 	= $this->input->post();
					$this->load->view('layouts/page_template', $page_data);
	            }else{
	            	if( strtoupper($action) == strtoupper('edit') ){
	            		if(is_null($user_id) || ($user_id == '')){
		            		show_404();
	            		}
	            	}//endif
					$this->manage_user($action, $page_data); //create or edit
	            }//endif
			}else{		
				if( strtoupper($action) == strtoupper('edit') ){			
					if($page_data['user_data'] && array_key_exists('USER_ID', $page_data['user_data'])){
						$page_data['user_data']['CONFIRM_PASSCODE'] = $page_data['user_data']['PASSCODE'];
					}else{
						show_404();
					}//endif
				}//endif
				$this->load->view('layouts/page_template', $page_data);
			}//endif			
		}else{
			show_404();
		}//endif
	}//end index()

	/**
	* @desc check username if already exists
	* @param string $username
	* @return bool
	*/
	public function has_user($username){
		if($username){
			$check_res = $this->m_user->has_user($username);
			return ($check_res?true:false);
		}//endif
	}//end check_user()

	/**
	* @desc get user system accesses 
	* @param int user_id
	* @return json
	*/
	public function get_user_system_access($user_id, $user_source_id){
		$get_res 	= $this->m_system->get_user_system_access($user_id, $user_source_id);
		$response 	= [];
		if( $get_res ){
			$response = array('status'=> true, 'data' => $get_res);
		}else{
			$response = array('status'=> false, 'data'=> []);
		}//endif
		echo json_encode($response);
		exit;
	}//end get_user_system_access()

	/**
	* @desc update user system accesses
	* @param int access_id
	* @return json
	*/
	public function update_system_access(){
		$response 								= [];
		$update_data 							= $this->input->post();
		$access_id 								= $this->input->post('access_id');
		$update_data['update_option'] 			= ($access_id ? 'update' : 'insert');
		$update_data['managed_by'] 				= $this->session->userdata('user_id');
		$update_data['manager_user_source_id'] 	= $this->session->userdata('user_source_id');
		$update_data['STATUS_ID'] 				= 1;
		$update_res 							= $this->m_system->update_user_access($update_data);
		if( isset($update_data['system_id']) && array_key_exists('system_id', $update_data) ){	
			$input_sys_id = $this->input->post('system_id');
			if($input_sys_id && $input_sys_id != ''){			
				if( $update_res && $update_data['update_option'] == 'insert' ){
					$response = array('status'=> true, 'data' => $update_res); //pass the newly inserted id to update checkbox access id
				}else{
					$response = array('status'=> true, 'data'=> $access_id);
				}//endif
			}else{
				$response = array('status'=> false, 'data'=> []);
			}//endif
		}else{
			$response = array('status'=> false, 'data'=> []);
		}//endif
		echo json_encode($response);
		exit;
	}//end update_system_access()

	/**
	* @desc manage user | edit or delete
	* @param int access_id
	* @return json
	*/
	private function manage_user($action, $page_data){
		$input_arr 							= $this->input->post();
		$user_name 							= $this->input->post('USER_NAME');
		$input_arr['MANAGE_BY']				= $this->session->userdata('user_id'); 
		$input_arr['MANAGER_SOURCE_ID'] 	= $this->session->userdata('user_source_id'); 
		$input_arr['STATUS_ID'] 			= 1; 
		$action 							= strtoupper($action);
		$user_exists  						= $this->has_user($user_name);

		switch ($action) {
			case 'CREATE':

					if( !$this->has_user($user_name) ){			
						$insert_res = $this->m_user->manage($input_arr, $action);
						if( $insert_res ){
							//successful insert
							$this->flash_message('success', 'Successfully added.');
							redirect('user');
						}else{
							show_404();
						}//endif
					}

				break;
			case 'EDIT':

					$page_data['user_data']['CONFIRM_PASSCODE'] = $page_data['user_data']['PASSCODE'];
					$input_arr['USER_ID'] = $page_data['user_data']['USER_ID'];
					if( !$user_exists || $page_data['user_data']['USER_NAME'] == $this->input->post('USER_NAME') ){	
							
						$update_res = $this->m_user->manage($input_arr, $action);
						if( $update_res ){
							//successful update
							$this->flash_message('success', 'Successfully updated.');
							redirect('user');
						}//endif
					}

				break;
			default:
				show_404();
				break;
		}//end switch

		$this->flash_message('danger', 'The username <i>"'. $this->input->post('USER_NAME').'"</i> already exists.');
		$page_data['user_data'] 	= $this->input->post();
		$this->load->view('layouts/page_template', $page_data);

	}//end manage_user()

}//end class
