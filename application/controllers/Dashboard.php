<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('system_model', 'm_system');
	}//end __construct()

	/**
	* @desc Index Page for this controller.
	* @author John Joey C. Abuedo | IPC
	* @see link https://mis_isuzu@bitbucket.org/mis_isuzu/webapps.git 
	* @return view
	*/
	public function index(){
		// echo '<pre>';print_r($this->session->userdata() );die;
		$user_id						= $this->session->userdata('user_id'); 
		$source_id 						= $this->session->userdata('user_source_id'); 
		$page_data['systems'] 			= $this->m_system->get_system();
		$page_data['user_access']  		= $this->m_system->get_user_system_access($user_id, $source_id);
		$page_data['page_content']  	= 'dashboard/index';
		$page_data['page_title']  		= 'Dashboard';
		$page_data['content_title'] 	= 'IPC Web Applications';
		$this->load->view('layouts/page_template', $page_data);
	}//end index()

}//end class
