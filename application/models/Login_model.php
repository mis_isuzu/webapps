<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class login_model extends CI_Model {

	/**
	* @desc validate user credentials
	* @param string username
	* @param string password
	* @return array
	*/
	public function validate_user($data){
		// $params = array('', 'password', 'jj', 'password');
		//$params = array($data['username'], 'kalabasa1','150603', 'qwqw');
		$sql = 	"SELECT *
					FROM (
			        SELECT 	usr.user_id,
				            usr.user_name,
				            1 status_id,
				            null password_expiration,
				            ppf.first_name,
				            ppf.middle_names middle_name,
				            ppf.last_name,
				            ppf.full_name,
				            ppf.attribute2 division,
				            ppf.attribute3 department,
				            ppf.attribute4 section,
				            ppf.employee_number employee_number,
				            1 user_source_id
			        FROM APPS.fnd_user usr LEFT JOIN APPS.per_all_people_f ppf
						ON usr.employee_id = ppf.person_id
			        WHERE usr.user_name = ?
						AND usr.end_date IS NULL
						AND APPS.IPC_DECRYPT_ORA_USR_PWD(usr.encrypted_user_password) = ?
			        UNION
			        SELECT 	usr.user_id,
							usr.user_name,
							usr.status_id,
							usr.password_expiration,
							ud.first_name,
							ud.middle_name,
							ud.last_name,
							null full_name,
							ud.division,
							ud.department,
							ud.section,
							null employee_number,
							2 user_source_id
			        FROM IPC_PORTAL.users usr
			        	 LEFT JOIN IPC_PORTAL.user_details ud
			        	 	ON ud.user_id = usr.user_id 
			        WHERE usr.user_name = ?
					AND usr.passcode = ?)
				";
		$query 	= $this->db->query($sql, $data);
		$res 	= $query->row();
		//echo '<pre>';print_r($res);die;
		if( count( $res ) > 0 ){
			return $res;
		}//endif
		return false;
	}//end validate_user()

	/**
	* @desc change password for specific user
	* @param string user_name
	* @param string old_password
	* @param string new_password
	* @param string confirm_password
	* @return bool
	*/
	public function change_pw($data){
		// echo '<pre>';print_r($data);die;
		$sql_data 	= array(
				'passcode' 		=> $data['new_password'],
				'user_name' 	=> $data['user_name'],
				'old_password' 	=> $data['old_password'],
			);

		$this->db->trans_begin();
			$sql = "UPDATE IPC_PORTAL.users u
						SET u.passcode 	= ?, 
							u.password_expiration = add_months(SYSDATE, +".$this->$config->item('user_pw_expiration').")
					    WHERE u.user_name = ? AND u.passcode = ?
					";
			$this->db->query($sql, $sql_data);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE){	
	        $this->db->trans_rollback();
	        return false;
		}else{
	        $this->db->trans_commit();
	        return true;
		}//endif	

	}//end change_pw()

}//end class
