<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User_model extends CI_Model {

	/**
	* @desc get all user
	* @return array
	*/
	public function get_all_user(){
		$sql = 	"SELECT *
					FROM (
			        SELECT 	usr.user_id,
				            usr.user_name,
				            ppf.first_name,
				            ppf.middle_names middle_name,
				            ppf.last_name,
				            ppf.full_name,
				            ppf.attribute2 division,
				            ppf.attribute3 department,
				            ppf.attribute4 section,
				            ppf.attribute4 employee_number,
				            1 user_source_id,
				            1 status_id
			        FROM APPS.fnd_user usr 
			        LEFT JOIN APPS.per_all_people_f ppf
						ON usr.employee_id = ppf.person_id
			        WHERE 	usr.end_date IS NULL
						--AND APPS.IPC_DECRYPT_ORA_USR_PWD(usr.encrypted_user_password) = ?
			        UNION
			        SELECT 	usr.user_id,
							usr.user_name,
							ud.first_name,
							ud.middle_name,
							ud.last_name,
							null full_name,
							ud.division,
							ud.department,
							ud.section,
							null employee_number,
							2 user_source_id,
							usr.status_id
			        FROM IPC_PORTAL.users usr
		        		LEFT JOIN IPC_PORTAL.user_details ud
			        	 	ON ud.user_id = usr.user_id 
			        --WHERE ud.status_id = 1
					--AND usr.passcode = ?
					)
				WHERE first_name IS NOT NULL
						AND last_name IS NOT NULL
				ORDER BY first_name ASC
				";
		$query 	= $this->db->query($sql);
		$res 	= $query->result();
		if( count( $res ) > 0 ){
			return $res;
		}//endif
		return false;
	}//end validate_user()

	/**
	* @desc get all user
	* @return array
	*/
	public function get_user( $user_id = null ){
		if( !is_null($user_id) && is_numeric($user_id) ){		
			$sql = 	"	SELECT 	usr.user_id,
								usr.user_name,
								usr.passcode,
								ud.first_name,
								ud.middle_name,
								ud.last_name,
								null full_name,
								ud.division,
								ud.department,
								ud.section,
								ud.position_title,
								ud.organization,
								ud.division,
								usr.status_id,
								2 user_source_id
				        FROM IPC_PORTAL.users usr
				        	 LEFT JOIN IPC_PORTAL.user_details ud
				        	 	ON ud.user_id = usr.user_id 
				        WHERE usr.user_id = ?
					";
			$query 	= $this->db->query($sql, $user_id);
			$res 	= $query->row();
			if( count( $res ) > 0 ){
				return $res;
			}//endif
		}//endif
		return false;
	}//end validate_user()

	/**
	* @desc insert/edit user
	* @return array
	*/
	public function manage($data, $action){

		$sql_data1 	= array(
						'user_name' 			=> $data['USER_NAME'],
						'passcode' 				=> $data['PASSCODE'],
						'status_id' 			=> $data['STATUS_ID'],
						'created_by' 			=> $data['MANAGE_BY'],
						'create_user_source_id' => $data['MANAGER_SOURCE_ID'],
					);
		$sql_data2 	= array(
						'first_name' 			=> $data['FIRST_NAME'],
						'middle_name' 			=> $data['MIDDLE_NAME'],
						'last_name' 			=> $data['LAST_NAME'],
						'status_id' 			=> $data['STATUS_ID'],
						'managed_by' 			=> $data['MANAGE_BY'],
						'manage_user_source_id' => $data['MANAGER_SOURCE_ID'],	
						'section' 				=> $data['SECTION'],		
						'department' 			=> $data['DEPARTMENT'],		
						'division' 				=> $data['DIVISION'],		
						'position_title' 		=> $data['POSITION_TITLE'],		
						'organization' 			=> $data['ORGANIZATION'],		
					);
		$this->db->trans_begin();


			//INSERT USER
			if( strtoupper($action) == strtoupper('create') ){
				$sql1 	= "	INSERT INTO IPC_PORTAL.users (
							    user_id,
							    customer_id,
							    user_name,
							    passcode,
							    status_id,
							    created_by,
							    create_user_source_id,
							    date_created,
							    password_expiration
							)
							VALUES (
							    IPC_PORTAL.users_seq.nextval, -- user_id
							    null, -- customer_id
							    ?, -- user_name
							    ?, -- passcode
							    ?, -- status_id
							    ?, -- user_id
							    ?, -- user_source_id
							    SYSDATE,
							    add_months(SYSDATE, +".$this->config->item('user_pw_expiration').")
							)";
				$sql2 	= "	INSERT INTO IPC_PORTAL.user_details (
							    user_detail_id,
							    user_id,
							    first_name,
							    middle_name,
							    last_name,
							    status_id,
							    created_by,
							    create_user_source_id,
							    date_created,
							    section,
							    department,
							    division,
							    position_title,
							    organization
							)
							VALUES (
							    IPC_PORTAL.user_details_seq.nextval, -- user_detail_id
							    IPC_PORTAL.users_seq.currval, -- user_id
							    ?, -- first_name
							    ?, -- middle_name
							    ?, -- last_name
							    ?, -- status_id
							    ?, -- created_by
							    ?, -- create_user_source_id
							    SYSDATE,
							    ?, -- section
							    ?, -- department
							    ?, -- division
							    ?, -- position_title
							    ? -- organization
							)";
			//EDIT USER
			}else{

				$sql1 = "UPDATE IPC_PORTAL.users u
							SET u.user_name 				= ?,
							    u.passcode  				= ?,
							    u.status_id 				= ?,
							    u.updated_by 				= ?,
							    u.update_user_source_id		= ?,
							    u.date_updated 				= SYSDATE
						    WHERE u.user_id 				= ".$data['USER_ID']."
				";

				$sql2 = "UPDATE IPC_PORTAL.user_details ud
							SET ud.first_name 				= ?,
							    ud.middle_name 				= ?,
							    ud.last_name 				= ?,
							    ud.status_id 				= ?,
							    ud.updated_by 				= ?,
							    ud.update_user_source_id 	= ?,
							    ud.date_updated 			= SYSDATE,
							    ud.section 					= ? ,
							    ud.department 				= ?,
							    ud.division 				= ?,
							    ud.position_title 			= ?,
							    ud.organization 			= ?
						    WHERE ud.user_id 				= ".$data['USER_ID']."
				";
			}

			$this->db->query($sql1, $sql_data1);
			$this->db->query($sql2, $sql_data2);
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE){	
	        $this->db->trans_rollback();
	        return false;
		}else{
	        $this->db->trans_commit();
	        return true;
		}//endif
	}//end insert()

	/**
	* @desc check username if already exists
	* @param string $username
	* @return bool
	*/
	public function has_user($username){
		$data = array($username, $username);
		$sql = 	"SELECT *
					FROM (
			        SELECT 	usr.user_id
			        FROM APPS.fnd_user usr
			        WHERE usr.end_date IS NULL
			        	AND usr.user_name = ?
			        UNION
			        SELECT 	usr.user_id
			        FROM IPC_PORTAL.users usr
			        WHERE usr.user_name = ?
					)
				";
		$query 	= $this->db->query($sql, $data);
		$res 	= $query->row();
		if( count( $res ) > 0 ){
			return $res;
		}//endif
		return false;
	}
}//end class
