<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class System_model extends CI_Model {

	/**
	* @desc get all systems(oracle)
	* @return array
	*/
	public function get_system(){
		$sql 	= 	"SELECT 	
						sys.system_name,
						sys.system_id,
						sys.hierarchy,
						sys.url
			        FROM IPC_PORTAL.systems sys 
			        WHERE sys.status_id = ?
			        ORDER BY sys.hierarchy ASC
				";
		$query 	= $this->db->query( $sql, array(1) );
		$res 	= $query->result();
		if( count( $res ) > 0 ){
			return $res;
		}//endif
		return false;
	}//end validate_user()

	/**
	* @desc get all systems user types(oracle)
	* @return array
	*/
	public function get_system_usertypes(){
		$sql 	= 	"SELECT 	
						sys_u.user_type_id,
						sys_u.user_type_name,
						sys_u.system_id
			        FROM IPC_PORTAL.system_user_types sys_u 
			        WHERE sys_u.status_id = ?
				";
		$query 	= $this->db->query( $sql, array(1) );
		$res 	= $query->result();
		if( count( $res ) > 0 ){
			return $res;
		}//endif
		return false;
	}//end validate_user()

	/**
	* @desc get all systems user types(oracle)
	* @return array
	*/
	public function get_user_system_access($user_id, $user_source_id){
		$sql 	= 	"SELECT 	
						user_acc.access_id,						
						user_acc.system_id,						
						user_acc.user_type_id,
						user_acc.status_id,						
						user_acc.user_id,						
						user_acc.user_source_id					
			        FROM IPC_PORTAL.user_system_access user_acc 
			        WHERE user_acc.user_id = ? 
			        	AND user_acc.user_source_id = ? 
				";
		$query 	= $this->db->query( $sql, array($user_id, $user_source_id) );
		$res 	= $query->result();
		if( count( $res ) > 0 ){
			return $res;
		}//endif
		return false;
	}//end validate_user()

	/**
	* @desc UPDATE USER ACCESS
	* @return array
	*/
	public function update_user_access($data){
		$update_op = $data['update_option'];
		$this->db->trans_begin();
			if( $update_op == 'update' ){		
				//echo 'update';
				$sql_data 	= array( 
								$data['status_id'],
								$data['user_type_id'],
								$data['managed_by'], 
								$data['manager_user_source_id'], 
								$data['access_id'],
				  			);
				$sql 			= "UPDATE IPC_PORTAL.user_system_access user_acc	
									SET user_acc.status_id = ?,
										user_acc.user_type_id = ?, 				
										user_acc.updated_by = ?, 				
										user_acc.update_user_source_id = ?, 				
										user_acc.date_updated = SYSDATE				
									WHERE user_acc.access_id = ?
								";
			}else{
				//echo 'insert';
				$sql_data = array( 
									$data['system_id'], 
									$data['user_type_id'], 
									$data['user_id'], 
									$data['managed_by'], 
									$data['manager_user_source_id'], 
									$data['status_id'], 
									$data['source_id'], 
								);
				$sql 	= "	INSERT INTO ipc_portal.user_system_access (
							    access_id,
							    system_id,
							    user_type_id,
							    user_id,
							    created_by,
							    create_user_source_id,
							    date_created,
							    status_id,
							    user_source_id
							)
							VALUES (
							    ipc_portal.user_system_access_seq.nextval, -- access_id
							    ?, -- system_id
							    ?, -- user_type_id
							    ?, -- user_id
							    ?, -- created_by
							    ?, -- create_user_source_id
							    SYSDATE, --date_created
							    ?, -- status_id
							    ?
							)";

			}//endif
			$this->db->query( $sql, $sql_data );
			$id = $this->getNextId('ipc_portal.user_system_access');
		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE){
	        $this->db->trans_rollback();
	        return false;
		}else{
	        $this->db->trans_commit();
	        return ($update_op == 'insert'? $id: true);
		}//endif
	}//end validate_user()

	//get incremental id
	private function getNextId($table) {
		$sql 	= 	'SELECT 	
						ipc_portal.user_system_access_seq.currval AS NEXTID					
			        FROM ipc_portal.user_system_access
				';
		$query 	= $this->db->query( $sql );
		$res 	= $query->row();
		return $res->NEXTID;
	} 

	//check if user has permission on specific system
	public function check_system_permission($data){
		$sql 	= 	"SELECT 	
						user_acc.access_id							
			        FROM IPC_PORTAL.user_system_access user_acc 
			        WHERE user_acc.user_id = ? 
			        	AND user_acc.system_id = ? 
			        	AND user_acc.user_source_id = ? 
			        	AND user_acc.status_id = ? 
		";
		$query 	= $this->db->query( $sql, $data );
		$res 	= $query->result();
		if( count( $res ) > 0 ){
			return $res;
		}//endif
		return false;
	}

}//end class
