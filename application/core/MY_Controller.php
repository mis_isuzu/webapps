<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function __construct(){
		parent::__construct();
		// $CI = & get_instance();
		// $CI->load->library('session');
		// $CI->load->helper('url');
		// $CI->load->helper('form');
		// $user_data = $this->session->userdata('user_id');
		// if (!isset($user_data)){ 
		// 	redirect(base_url('../webapps'));
		// }
		$CI = & get_instance();
		$CI->load->library('ipc_login_validation');
		$CI->ipc_login_validation->is_logged_in();
	}

	public function check_system_permission(){
		$CI = & get_instance();
		$CI->load->model('system_model', 'm_system');
		$session_data 	= $CI->session->userdata();
		$check_data		= array(
								'user_id' 	=> $session_data['user_id'],
								'system_id' => 3, // 3 = IPC USER MANAGEMENT
								'source_id' => $session_data['user_source_id'],
								'status_id' => 1
							);
		
		$check_result 	= $CI->m_system->check_system_permission($check_data);
		if($check_result){
			return true;
		}else{
			echo show_404();
		}
	}

	/**
	* @desc create flash data
	* @param string $alert_type bootstrap
	* @param string $msg message
	*/
	public function flash_message($alert_type, $msg){
		$CI = & get_instance();
		$CI->session->set_flashdata('msg', array('alert_type' => $alert_type, 'msg' => $msg) );
	}
}