$(document).ready(function(){
    userDt = $('#user-datatables').DataTable();

    $('#user-datatables tbody').on( 'click', 'tr', function () {
        var complete_name 	= 	(userDt.row( this ).data()['1'] != '' ? 
	        						userDt.row( this ).data()['1'] : 
	        						'Name not available in database'
        						);
        var division 		= 	(userDt.row( this ).data()['2'] != '' ?	
									userDt.row( this ).data()['2'] : 
									'Division not available in database'
        						 );
        var user_id 		= 	userDt.row( this ).data()['0'];
        var user_source_id 	= 	(userDt.row( this ).data()['5'] == 'ipc_portal' ? 2 : 1);
        $('#m_access_modal .modal-content .modal-body').find('#user_name').html(complete_name);
        $('#m_access_modal .modal-content .modal-body').find('#division_name').html(division);
        set_user_id( user_id, user_source_id );
        __ajax_get_user_system_access( user_id, user_source_id );
    });

	$('.switch_system_access').bootstrapSwitch();
	// $('.switch_system_access').on('click', function (){
	// 	var is_checked = $(this).is(':checked');
	// 	var userTypeEl = $(this).parent().parent().parent().parent().next().next().find('.user_types');
	// 	if(is_checked){
	// 		userTypeEl.removeAttr('disabled');
	// 	}else{
	// 		userTypeEl.attr('disabled', 'disabled');
	// 	}//endif
	// 	//console.log($(this).parent().parent().parent().parent().next().next().find('.user_types').prop('disabled', false));
	// 	update_system_access( $(this) );
	$('.bootstrap-switch-container').find('span').each(function(){
		$(this).attr('onclick', 'update_system_access(this, "switch")');
	});

	$('.user_types').on('change', function(e){
		update_system_access($(this), "select");
	});
});

//set user id for each bootstrap switch and select user type (per row)
function set_user_id(user_id, user_source_id){ //todo
	$('#user_system_access_tbl tbody td').each( function(){
		var switchEl 	= $(this).find('.switch_system_access');
		var switchId 	= switchEl.attr('id');
		var selEl 		= $(this).find('.user_types');
		switchEl.attr('data-user-id', user_id);
		switchEl.attr('data-source-id', user_source_id);
		selEl.attr('data-user-id', user_id);
		selEl.attr('data-source-id', user_source_id);
		//selEl.attr('data-system-id', switchEl.attr('data-system-id'));
	});
}

function __ajax_get_user_system_access(user_id, user_source_id){

	$.ajax({
		url: BASE_URL+ 'user/get_user_system_access/'+user_id+'/'+ user_source_id,
		dataType: 'JSON',
		beforeSend: htmlLoader('.access_tr_main_wrapper', '#access_loader ', 'show'),
		success: (function(data){
			var userAccess  = data['data'];
			
			$('#user_system_access_tbl tbody tr td').each( function(){

				var curSwitchEl  		= $(this).find('.switch_system_access');
				var curSwitchId 		= curSwitchEl.attr('id');
				var curSwitchUserId 	= curSwitchEl.attr('data-user-id');
				var curSwitchSourceId 	= curSwitchEl.attr('data-source-id');
				var userTypesEl 	= $(this).find('.user_types');
				var userTypesId 	= userTypesEl.attr('id');
				var switchUserId 	= userTypesEl.attr('data-user-id');
				var switchSourceId 	= userTypesEl.attr('data-source-id');

				if( userAccess.length > 0 ){

					for( x in userAccess ){
						var accessSystemId 	= userAccess[x]['SYSTEM_ID'];
						var accessID 		= userAccess[x]['ACCESS_ID'];
						var accessStatus 	= userAccess[x]['STATUS_ID'];
						var accessUserId 	= userAccess[x]['USER_ID'];
						var accessSourceId 	= userAccess[x]['USER_SOURCE_ID'];

						//enable/disable bootstrap switch based on user access
						if( 'switch_system_access'+accessSystemId == curSwitchId ){

							if(curSwitchUserId == accessUserId && curSwitchSourceId == accessSourceId){	

								curSwitchEl.attr('data-access-id', accessID);
								if(accessStatus == 1){
									curSwitchEl.bootstrapSwitch('state', true);
									return true;
								}else{
									curSwitchEl.bootstrapSwitch('state', false);
									return true;
								}//endif

							}else{
								curSwitchEl.bootstrapSwitch('state', false);
							}//endif

						}else{
							curSwitchEl.bootstrapSwitch('state', false);
						}//endif

						//select user type based on user access
						if( 'user_types'+accessSystemId == userTypesId ){
							userTypesEl.prop('disabled', false);
							if( switchUserId == accessUserId && switchSourceId == accessSourceId ){

								userTypesEl.attr('data-access-id', accessID);
								userTypesEl.find('option').removeAttr('selected');
								$(document).find('#'+userTypesId+' option[value='+userAccess[x]['USER_TYPE_ID']+']').prop('selected', true);
								
								if( accessStatus == 1 ){
									//set option for user type select 
									userTypesEl.prop('disabled', false);
									return true;
								}else{
									userTypesEl.prop('disabled', true);
									return true;
								}//endif

							}else{
								userTypesEl.prop('disabled', true);
							}//endif

						}else{
							userTypesEl.prop('disabled', true);
						}//endif

					}//endfor
				}else{
					curSwitchEl.bootstrapSwitch('state', false);
					curSwitchEl.attr('data-access-id', 0);
					userTypesEl.prop('disabled', true);
					userTypesEl.attr('data-access-id', 0);
				}//endif
			});
		}),
		complete: htmlLoader('.access_tr_main_wrapper', '#access_loader ', 'hide'),
	});

}

//change user type(select) state
function change_ut_state(me){
    var is_check = $(me).find('.switch_system_access').is( ":checked" );
    var selector = $(me).parent().next().next().find('select.user_types');
    (is_check ? selector.prop('disabled', false) : selector.prop('disabled', true))
}

function reset_bootstrap_switch(){
	$('.switch_system_access').attr('data-access-id', 0);
	$('.switch_system_access').bootstrapSwitch('state', false);
}

function update_system_access(me, el_type){
	var statusId;
	var accessId;
	var userId;
	var sourceId;
	var userTypeId;
	var systemId;
	var dataObj;

	//element type; switch = bootstrap switch, select = user type
	switch( el_type ){
		case 'switch':
				var el    		= $(me).siblings('.switch_system_access');
				var is_checked 	= el.is(':checked');
				var userTypeEl 	= $(me).parent().parent().parent().parent().next().next().find('.user_types');
				if(is_checked){
					userTypeEl.removeAttr('disabled');
				}else{
					userTypeEl.attr('disabled', 'disabled');
				}//endif
				statusId 		= (el.is(':checked')?1:2);
				userTypeId  	= userTypeEl.val();
				systemId  		= el.attr('data-system-id');
			break;
		case 'select':
				var el    	= $(me);
				statusId 	= 1;
				userTypeId  = el.val();
				systemId  	= el.parent()
								.prev().prev()
								.find('.switch_system_access')
								.attr('data-system-id');
			break;
	}//endswitch
	
	accessId 	= el.attr('data-access-id');
	userId 		= el.attr('data-user-id');
	sourceId 	= el.attr('data-source-id');
	dataObj 	= 	{
						system_id: systemId,
						access_id: accessId,
						user_id: userId,
						source_id: sourceId,
						status_id: statusId,
						user_type_id :userTypeId
					}

	__ajax_update_system_access( dataObj, el, el_type );
}

//update user system access
function __ajax_update_system_access(dataObj, el, el_type){
	$.ajax({
		url : BASE_URL+'user/update_system_access',
		method :"POST",
		dataType: 'JSON',
		data: dataObj,
		success: (function(data){

			if( data['status'] ){
				el.attr('data-access-id', data['data']);

				if(el_type == 'switch'){
					var userTypeEl 	= el.parent().parent().parent().parent().next().next().find('.user_types');
					userTypeEl.attr('data-access-id', data['data']);
				}//endif

			}//endif
		})

	});
}