//== Class definition

var DatatableHtmlTableDemo = function() {
    //== Private functions

    // demo initializer
    var demo = function() {
        var datatable = $('#forecast-models-datatable').mDatatable({
            search: {
                input: $('#generalSearch')
            }
        });
        var datatable = $('#forecast-list-datatable').mDatatable();
    };
    return {
        //== Public functions
        init: function() {
            // init dmeo
             demo();
        },
    };
}();

jQuery(document).ready(function() {
    DatatableHtmlTableDemo.init();
});